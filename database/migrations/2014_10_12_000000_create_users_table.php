<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('plain_password');
            $table->integer('type_id');
            $table->integer('package_id');
            $table->string('server_ip');
            $table->integer('inode');
            $table->boolean('has_backup');
            $table->boolean('has_shellaccess');
            $table->integer('no_of_processes');
            $table->integer('no_of_openfiles');
            $table->boolean('is_deleted');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
