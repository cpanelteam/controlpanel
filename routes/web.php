<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::get('/', 'DashboardController@index');
Route::get('/dashboard', 'DashboardController@index');

Route::get('useraccounts/listaccounts' , 'AccountController@index');
Route::get('useraccounts/newaccount' , 'AccountController@newAccountForm');
Route::post('useraccounts/addnewaccount' , 'AccountController@addNewAccount');

Route::get('useraccounts/listroles' , 'UserType@index');
Route::post('useraccounts/addnewrole' , 'UserType@addNewRole');
Route::get('useraccounts/newroles' , 'UserType@newRoleForm');
Route::post('useraccounts/getDashboardGraph' , 'UserType@getDashboardGraph');
Route::post('useraccounts/saveDashboardGraph' , 'UserType@saveDashboardGraph');
Route::post('useraccounts/getRoleMenu' , 'UserType@getRoleMenu');
Route::post('useraccounts/getMenuList' , 'UserType@getMenuList');
Route::post('useraccounts/saveRoleMenu' , 'UserType@saveRoleMenu');

Route::get('useraccounts/fixpermissions' , 'DashboardController@fixPermissions');

Route::get('useraccounts/cpanelmigration' , 'DashboardController@cpanelMigration');

Route::get('packages/addpackage' , 'PackageController@newPackageForm');
Route::get('packages/listpackages' , 'PackageController@listPackages');
