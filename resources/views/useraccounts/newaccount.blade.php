@include('templates.header')

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>New Account
                    <!--<small>Some examples to get you started</small>-->
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Create New Account</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br/>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li >{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"
                              action="{{ url('useraccounts/addnewaccount') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fullname">Full Name <span
                                            class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="fullname" name="fullname" required="required" value="@if(count($userObject) > 0){{$userObject->name}}@endif"
                                           pattern=".{5,}" class="form-control col-md-7 col-xs-12" title="Full name must be greater than 5 characters">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username <span
                                            class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="username" name="username" required="required" value="@if(count($userObject) > 0){{$userObject->username}}@endif"
                                           pattern=".{5,}" class="form-control col-md-7 col-xs-12" title="Username must be greater than 5 characters">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password <span
                                            class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="password" value="@if(count($userObject) > 0){{$userObject->plain_password}}@endif"
                                           pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
                                           name="password" required="required" class="form-control col-md-7 col-xs-12" title="Password must be >= 8 characters, it should contain upper case letter, lower case letter and numeric letter">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_type">User type
                                    </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="user_type" name="user_type" class="select2_group form-control">
                                        @foreach($user_types as $user_type)
                                            <option @if(count($userObject) > 0 && $userObject->user_type == $user_type->user_type_id) selected="selected" @endif value="{{$user_type->user_type_id}}">{{$user_type->user_type_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="server_ips">Server
                                    IPs</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="server_ips" name="server_ips" class="select2_group form-control">
                                        <option @if(count($userObject) > 0 && $userObject->server_ips == "AK") selected="selected" @endif value="AK">Alaska</option>
                                        <option @if(count($userObject) > 0 && $userObject->server_ips == "HI") selected="selected" @endif value="HI">Hawaii</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="package">Package</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="package" name="package" class="select2_group form-control">
                                        <option @if(count($userObject) > 0 && $userObject->package == 1) selected="selected" @endif value="1">Alaska</option>
                                        <option @if(count($userObject) > 0 && $userObject->package == 2) selected="selected" @endif value="2">Hawaii</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="inode">Inode <span
                                            class="required">*</span>
                                </label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <input type="text" id="inode" name="inode" required="required" value="@if(count($userObject) > 0){{$userObject->inode}}@endif"
                                           pattern="[0-9]+" class="form-control col-md-7 col-xs-12" title="Must be numeric only">
                                </div>
                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="inode">(Limit inodes, 0
                                    for unlimited) <span class="required">*</span>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span
                                            class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="email" id="email" name="email" required="required" value="@if(count($userObject) > 0){{$userObject->email}}@endif"
                                           class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="backup">Backup <span
                                            class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="backup" name="backup" type="checkbox" value="1" @if(count($userObject) > 0 && $userObject->backup == 1) checked @endif > ( Backup user )
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="shell_access">Shell Access
                                    <span
                                            class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="shell_access" name="shell_access" type="checkbox" value="1" @if(count($userObject) > 0 && $userObject->shell_access == 1) checked @endif>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="processes">Processes <span
                                            class="required">*</span>
                                </label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <input type="text" id="processes" name="processes" required="required" value="@if(count($userObject) > 0){{$userObject->processes}}@endif"
                                           pattern="[0-9]+" class="form-control col-md-7 col-xs-12" title="Must be numeric only">
                                </div>
                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="processes">(Limit number
                                    of processes) <span class="required">*</span>
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="open_files">Open Files
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <input type="text" id="open_files" name="open_files" required="required" value="@if(count($userObject) > 0){{$userObject->open_files}}@endif"
                                           pattern="[0-9]+" class="form-control col-md-7 col-xs-12" title="Must be numeric only">
                                </div>
                                <label class="control-label col-md-4 col-sm-4 col-xs-12" for="open_files">(Limit number
                                    of open files) <span class="required">*</span>
                                </label>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Create </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="clearfix"></div>

    </div>
</div>

@include('templates.footer')