@include('templates.header')

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Fix Permissions
                    <!--<small>Some examples to get you started</small>-->
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Fix Account Permissions</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br/>
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action=""
                              method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <a href="">Account Migration Instructions</a>
                            <p>This feature will restore cPanel backups if in one of the following formats:<br>
                                <br>
                                cpmove-{USER}.tar.gz<br>
                                Example account test: cpmove-test.tar.gz<br>
                                <br>
                                The files must be in one of the following directories on the server: /home<br>
                                You must upload the file to one of the above directories before you can use this feature.<br>
                                cPanel migration module will restore files from public_html folder, mysql databases and mysql users.<br>
                                <br>
                                List of .tar.gz files in the /home folder </p>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_id">Select User <span
                                            class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="user_id" name="user_id" class="select2_group form-control">
                                        <option value="AK">Alaska</option>
                                        <option value="HI">Hawaii</option>
                                    </select>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Fix Account Permissions</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="clearfix"></div>

    </div>
</div>

@include('templates.footer')