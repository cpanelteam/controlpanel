@include('templates.header')
<script>
    var role_id = '';
    var role_name = '';
    var menu_list = new Array();
    var graph_data = '';
    function showDashboardGraph(value, roleName) {
        $.ajax({
            type: 'POST',
            url: "{{ url('useraccounts/getDashboardGraph')  }}",
            data: {
                user_type_id: value
            },
            success: function (data) {
                $("#dashboard-graph-form").hide();
                role_id = value;
                role_name = roleName;
                $("#role-menu").hide();
                $("#dashboard-graph").show();
                $("#dashboard-graph-title").text(roleName);
                $("#dashboard-graph-edit-button").show();
                $("#dashboard-graph-list").show();
                data = jQuery.parseJSON(data);
                graph_data = data;
                if (data == '') {
                    $("#dashboard-graph-list").html("<p> No graph allotted till now.</p>");
                }else{
                    html = "";
                    dataCount = data.length;
                    for (i = 0; i < dataCount; i++) {
                        html += "<img style='width: 100%; display: block; margin-top: 5px;' src='" + data[i].user_type_dashboard_value + "' alt='image'/>";
                    }
                    $("#dashboard-graph-list").html(html);
                }
            }
        });
    }
    function editDashboardGraph() {
        $("#dashboard-graph-edit-button").hide();
        $("#dashboard-graph-list").hide();
        $("#dashboard-graph-save-button").show();
        for (i = 0; i < 4; i++) {
            $("#graph"+(i+1)+"-url").val("");
        }
        if(graph_data != ''){
            dataCount = graph_data.length;
            for (i = 0; i < dataCount; i++) {
                $("#graph"+(i+1)+"-url").val(graph_data[i].user_type_dashboard_value);
            }
        }
        $("#dashboard-graph-form").show();
    }
    function saveDashboardGraph() {
        $("#dashboard-graph-save-button").hide();
        dashboard_graph_data = [];
        for (i = 0; i < 4; i++) {
            graph_value = $.trim($("#graph"+(i+1)+"-url").val());
            if(graph_value != '') {
                dashboard_graph_data.push(graph_value);
            }
        }
        $.ajax({
            type: 'POST',
            url: "{{ url('useraccounts/saveDashboardGraph')  }}",
            data: {
                user_type_id: role_id,
                graph_urls: dashboard_graph_data
            },
            success: function (data) {
                showDashboardGraph(role_id, role_name);
                new PNotify({
                    title: 'Success',
                    text: 'Graph is successfully saved',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            }
        });
    }
    function showRoleMenu(value, roleName) {
        $.ajax({
            type: 'POST',
            url: "{{ url('useraccounts/getRoleMenu')  }}",
            data: {
                user_type_id: value
            },
            success: function (data) {
                menu_list = [];
                role_id = value;
                role_name = roleName;
                $("#dashboard-graph").hide();
                $("#role-menu").show();
                $("#role-title").text(roleName)
                $("#role-menu-save-button").hide();
                $("#role-menu-edit-button").show();
                menu = jQuery.parseJSON(data);
                if (menu != '') {
                    html = "";
                    menuSize = menu.length;
                    for (i = 0; i < menuSize; i++) {
                        if (!menu[i].menu_parent_name) {
                            menu_list.push(menu[i].menu_id);
                            html += "<p>- " + menu[i].menu_name + "</p>";
                        } else {
                            menu_parent_name = menu[i].menu_parent_name;
                            html += "<p>- " + menu[i].menu_parent_name + "</p>";
                            html += "<div class=\"col-sm-offset-1\">";
                            menu_list.push(menu[i].menu_id);
                            html += "<p>- " + menu[i].menu_name + "</p>";
                            while (i + 1 < menuSize && menu_parent_name == menu[i + 1].menu_parent_name) {
                                i++;
                                menu_list.push(menu[i].menu_id);
                                html += "<p>- " + menu[i].menu_name + "</p>";
                            }
                            html += "</div>";
                        }

                    }
                    $("#role-menu-list").html(html);
                } else {
                    $("#role-menu-list").html("<p> No menu allotted till now.</p>");
                }
            },
            error: function () {
                alert("failure");
            }
        });
    }
    function editRoleMenu() {
        $("#role-menu-edit-button").hide();
        $.ajax({
            type: 'POST',
            url: "{{ url('useraccounts/getMenuList')  }}",
            data: {},
            success: function (data) {
                $("#role-menu-save-button").show();
                menu = jQuery.parseJSON(data);
                html = "";
                menuSize = menu.length;
                for (i = 0; i < menuSize; i++) {
                    if (!menu[i].menu_parent_name) {
                        html += "<p><input type='checkbox' onclick='menuItemChanged(this," + menu[i].menu_id + ");'";
                        if (jQuery.inArray(menu[i].menu_id, menu_list) >= 0) {
                            html += "checked='checked'";
                        }
                        html += "> " + menu[i].menu_name + "</p>";
                    } else {
                        menu_parent_name = menu[i].menu_parent_name;
                        html += "<p>- " + menu[i].menu_parent_name + "</p>";
                        html += "<div class=\"col-sm-offset-1\">";
                        html += "<p><input type='checkbox' onclick='menuItemChanged(this," + menu[i].menu_id + ");'";
                        if (jQuery.inArray(menu[i].menu_id, menu_list) >= 0) {
                            html += "checked='checked'";
                        }
                        html += "> " + menu[i].menu_name + "</p>";
                        while (i + 1 < menuSize && menu_parent_name == menu[i + 1].menu_parent_name) {
                            i++;
                            html += "<p><input type='checkbox' onclick='menuItemChanged(this," + menu[i].menu_id + ");'";
                            if (jQuery.inArray(menu[i].menu_id, menu_list) >= 0) {
                                html += "checked='checked'";
                            }
                            html += "> " + menu[i].menu_name + "</p>";
                        }
                        html += "</div>";
                    }

                }
                $("#role-menu-list").html(html);
            }
        });
    }
    function menuItemChanged(ch, value) {
        if (ch.checked) {
            menu_list.push(value);
            console.log(menu_list);
        } else {
            menu_list.splice(menu_list.indexOf(value), 1);
            console.log(menu_list);
        }
    }
    function saveRoleMenu() {
        $("#role-menu-save-button").hide();
        $.ajax({
            type: 'POST',
            url: "{{ url('useraccounts/saveRoleMenu')  }}",
            data: {
                menu_id: menu_list,
                user_type_id: role_id
            },
            success: function (data) {
                showRoleMenu(role_id, role_name);
                new PNotify({
                    title: 'Success',
                    text: 'Menu is successfully saved',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            }
        });

    }
</script>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>New Role
                    <!--<small>Some examples to get you started</small>-->
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Roles</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <table class="table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Role Name</th>
                                <th>Menu</th>
                                <th>Dashboard Graphs</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($roles as $role)
                                <tr>
                                    <td>{{$role->user_type_id}}</td>
                                    <td>{{$role->user_type_name}}</td>
                                    <td>@if($role->user_type_id > 1)
                                            <a class="btn btn-info"
                                               href="javascript:showRoleMenu('{{$role->user_type_id}}' , '{{$role->user_type_name}}')">
                                                Show Menu </a>
                                        @endif</td>
                                    <td>
                                        <a class="btn btn-info"
                                           href="javascript:showDashboardGraph('{{$role->user_type_id}}' , '{{$role->user_type_name}}')">
                                            Show Dashboard Graphs </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12" id="role-menu" style="display:none;">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Roles Menu (<span id="role-title"></span>)</h2>
                        <ul class="nav navbar-right panel_toolbox" id="role-menu-button">
                            <a class="btn btn-info" href="javascript:editRoleMenu()" id="role-menu-edit-button"
                               style="display:none;">
                                Edit </a>
                            <a class="btn btn-info" href="javascript:saveRoleMenu()" id="role-menu-save-button"
                               style="display:none;">
                                Save </a>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-sm-offset-1" id="role-menu-list">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12" id="dashboard-graph" style="display:none;">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Dashboard Graph (<span id="dashboard-graph-title"></span>)</h2>
                        <ul class="nav navbar-right panel_toolbox" id="dashboard-graph-button">
                            <a class="btn btn-info" href="javascript:editDashboardGraph()"
                               id="dashboard-graph-edit-button"
                               style="display:none;">
                                Edit </a>
                            <a class="btn btn-info" href="javascript:saveDashboardGraph()"
                               id="dashboard-graph-save-button"
                               style="display:none;">
                                Save </a>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-sm-offset-1 col-sm-10" id="dashboard-graph-list" style="display:none;">
                        </div>
                        <div class="form-horizontal" id="dashboard-graph-form" style="display:none;">
                            <div class="form-group">
                                <label class="control-label col-sm-3 col-xs-12" for="graph1-url">Graph 1
                                    Url
                                </label>
                                <div class="col-sm-9 col-xs-12">
                                    <input type="text" id="graph1-url" name="graph1-url" required="required"
                                           class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 col-xs-12" for="graph2-url">Graph 2
                                    Url
                                </label>
                                <div class="col-sm-9 col-xs-12">
                                    <input type="text" id="graph2-url" name="graph2-url" required="required"
                                           class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 col-xs-12" for="graph3-url">Graph 3
                                    Url
                                </label>
                                <div class="col-sm-9 col-xs-12">
                                    <input type="text" id="graph3-url" name="graph3-url" required="required"
                                           class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 col-xs-12" for="graph4-url">Graph 4
                                    Url
                                </label>
                                <div class="col-sm-9 col-xs-12">
                                    <input type="text" id="graph4-url" name="graph4-url" required="required"
                                           class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="clearfix"></div>

    </div>
</div>

@include('templates.footer')