@include('templates.header')

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Packages
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>List Packages</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Package Name</th>
                                <th>Disk Quota</th>
                                <th>Bandwidth</th>
                                <th>FTP Accounts</th>
                                <th>Email Clients</th>
                                <th>Email Lists</th>
                                <th>Databases</th>
                                <th>Sub Domains</th>
                                <th>Parked Domains</th>
                                <th>Addon Domains</th>
                                <th>Hourly Emails</th>
                                <th>Edit Package</th>
                                <th>Delete Package</th>
                            </tr>
                            </thead>


                            <tbody>
                            <tr>
                                <td>default</td>
                                <td>1000</td>
                                <td>1000</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>10</td>
                                <td>200</td>
                                <td><a href="#">
                                        [Edit Package] </a></td>
                                <td><a class="btn btn-danger" href="#">
                                        Delete </a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="clearfix"></div>

    </div>
</div>

@include('templates.footer')