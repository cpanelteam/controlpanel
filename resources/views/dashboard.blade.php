@include('templates.header')
<script>
    function show_dashboard_div(value) {
        $.ajax({
            type: 'POST',
            url: "{{ url('settings/show_dashboard_div')  }}",
            data: {
                div: value
            },
            success: function (data) {
                $('#' + value + '_link').html("<a href=\"javascript:hide_dashboard_div('" + value + "');\">Hide</a>");
                $('#' + value + '_div').removeClass('opacity-2');
            },
            error: function () {
                alert("failure");
            }
        });
    }
    function hideDashboardGraph() {
        $("#dashboard-graph-div").hide();
        $.ajax({
            type: 'POST',
            url: "{{ url('settings/hideDashboardGraph')  }}",
            data: {
                div: value
            },
            success: function (data) {
                $('#' + value + '_link').html("<a href=\"javascript:show_dashboard_div('" + value + "');\">Show</a>");
                $('#' + value + '_div').addClass('opacity-2');
            },
            error: function () {
                alert("failure");
            }
        });
    }

    @if(count($graphs) > 0)
        var graph_data = new Array();
        @foreach($graphs as $graph)
            graph_data.push('{{$graph->user_type_dashboard_value}}');
        @endforeach
        var refresh_graps = setInterval(function(){
                refreshGraph()
        }, 10000);

        function refreshGraph() {
            var d = new Date();
            for(i = 0; i < {{count($graphs)}}; i++){
                $("#graph"+(i+1)).attr("src", graph_data[i] +"?"+d.getTime());
            }
        }
    @endif
</script>

<style>
    .stat_div .name_title, .stat_div p {
        margin: 0 0 2px;
    }
    .widget_profile_box img{
        height: 85px !important;
        width: 85px !important;
        margin: 0 0 4px 0 !important;
    }
</style>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Dashboard
                    <!--<small>Some examples to get you started</small>-->
                </h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12">

                <!-- graphs -->
                @if(count($graphs) > 0)
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel tile">
                                <div class="x_title">
                                    <h2>Health</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link" id="dashboard-graph-div"
                                               href="javascript:hideDashboardGraph()"><i
                                                        class="fa fa-chevron-up"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <?php $i = 1;?>
                                    @foreach($graphs as $graph)
                                        <div class="col-sm-6" style="margin-bottom: 10px"><img id="graph{{$i}}"
                                                    style="width: 100%; display: block;"
                                                    src="{{$graph->user_type_dashboard_value}}" alt="image"/></div>
                                            <?php $i++;?>
                                    @endforeach
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
            @endif
            <!-- /graphs -->
                <!-- stats -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel tile">
                            <div class="x_title">
                                <h2>Info</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="x_content txt_c stat_div">

                                        <div style="text-align: center;" class="widget_profile_box">
                                            <img src="{{ url('images/customers.png') }}" alt="..." class="img-circle profile_img">
                                        </div>

                                        <h4 class="name_title">Customers</h4>
                                        <p>1 used</p>
                                        <p>&nbsp;</p>

                                        <div class="divider"></div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="x_content txt_c stat_div">

                                        <div style="text-align: center;" class="widget_profile_box">
                                            <img src="{{ url('images/domains.png') }}" alt="..." class="img-circle profile_img">
                                        </div>

                                        <h4 class="name_title">Domains</h4>
                                        <p>1 used</p>
                                        <p>&nbsp;</p>

                                        <div class="divider"></div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="x_content txt_c stat_div">

                                        <div style="text-align: center;" class="widget_profile_box">
                                            <img src="{{ url('images/domains.png') }}" alt="..." class="img-circle profile_img">
                                        </div>

                                        <h4 class="name_title">SubDomains</h4>
                                        <p>1 used</p>
                                        <p>0 assigned</p>

                                        <div class="divider"></div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="x_content txt_c stat_div">

                                        <div style="text-align: center;" class="widget_profile_box">
                                            <img src="{{ url('images/domains.png') }}" alt="..." class="img-circle profile_img">
                                        </div>

                                        <h4 class="name_title">Webspace (MiB)</h4>
                                        <p>1 used</p>
                                        <p>0 assigned</p>

                                        <div class="divider"></div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="x_content txt_c stat_div">

                                        <div style="text-align: center;" class="widget_profile_box">
                                            <img src="{{ url('images/domains.png') }}" alt="..." class="img-circle profile_img">
                                        </div>

                                        <h4 class="name_title">Traffic (GiB)</h4>
                                        <p>1 used</p>
                                        <p>0 assigned</p>

                                        <div class="divider"></div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="x_content txt_c stat_div">

                                        <div style="text-align: center;" class="widget_profile_box">
                                            <img src="{{ url('images/domains.png') }}" alt="..." class="img-circle profile_img">
                                        </div>

                                        <h4 class="name_title">MySql-Databases</h4>
                                        <p>1 used</p>
                                        <p>0 assigned</p>

                                        <div class="divider"></div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="x_content txt_c stat_div">

                                        <div style="text-align: center;" class="widget_profile_box">
                                            <img src="{{ url('images/domains.png') }}" alt="..." class="img-circle profile_img">
                                        </div>

                                        <h4 class="name_title">Email-lists</h4>
                                        <p>1 used</p>
                                        <p>0 assigned</p>

                                        <div class="divider"></div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="x_content txt_c stat_div">

                                        <div style="text-align: center;" class="widget_profile_box">
                                            <img src="{{ url('images/domains.png') }}" alt="..." class="img-circle profile_img">
                                        </div>

                                        <h4 class="name_title">Email-accounts</h4>
                                        <p>1 used</p>
                                        <p>0 assigned</p>

                                        <div class="divider"></div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="x_content txt_c stat_div">

                                        <div style="text-align: center;" class="widget_profile_box">
                                            <img src="{{ url('images/domains.png') }}" alt="..." class="img-circle profile_img">
                                        </div>

                                        <h4 class="name_title">Email-forworders</h4>
                                        <p>1 used</p>
                                        <p>0 assigned</p>

                                        <div class="divider"></div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="x_content txt_c stat_div">

                                        <div style="text-align: center;" class="widget_profile_box">
                                            <img src="{{ url('images/domains.png') }}" alt="..." class="img-circle profile_img">
                                        </div>

                                        <h4 class="name_title">FTP-accounts</h4>
                                        <p>1 used</p>
                                        <p>0 assigned</p>

                                        <div class="divider"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /stats -->

                <!--Top 5 Process-->
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Top 5 Process <a href="" class="blue">[live monitor]</a></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <!--<li class="dropdown">-->
                            <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>-->
                            <!--<ul class="dropdown-menu" role="menu">-->
                            <!--<li><a href="#">Settings 1</a>-->
                            <!--</li>-->
                            <!--<li><a href="#">Settings 2</a>-->
                            <!--</li>-->
                            <!--</ul>-->
                            <!--</li>-->
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="table-responsive x_content">

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>PID</th>
                                <th>USER</th>
                                <th>PR</th>
                                <th>NI</th>
                                <th>VIRT</th>
                                <th>RES</th>
                                <th>SHR</th>
                                <th>S</th>
                                <th>%CPU</th>
                                <th>%MEM</th>
                                <th>TIME+</th>
                                <th>COMMAND</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>root</td>
                                <td>20</td>
                                <td>0</td>
                                <td>19360</td>
                                <td>1524</td>
                                <td>1228</td>
                                <td>S</td>
                                <td>0.0</td>
                                <td>0.1</td>
                                <td>0:01.27</td>
                                <td>init</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>root</td>
                                <td>20</td>
                                <td>0</td>
                                <td>19360</td>
                                <td>1524</td>
                                <td>1228</td>
                                <td>S</td>
                                <td>0.0</td>
                                <td>0.1</td>
                                <td>0:01.27</td>
                                <td>init</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>root</td>
                                <td>20</td>
                                <td>0</td>
                                <td>19360</td>
                                <td>1524</td>
                                <td>1228</td>
                                <td>S</td>
                                <td>0.0</td>
                                <td>0.1</td>
                                <td>0:01.27</td>
                                <td>init</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                <!--/Top 5 Process-->
                <!--Disk Details-->
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Disk Details <a href="" class="blue">(disk details)</a></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <!--<li class="dropdown">-->
                            <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>-->
                            <!--<ul class="dropdown-menu" role="menu">-->
                            <!--<li><a href="#">Settings 1</a>-->
                            <!--</li>-->
                            <!--<li><a href="#">Settings 2</a>-->
                            <!--</li>-->
                            <!--</ul>-->
                            <!--</li>-->
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="table-responsive x_content">

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Filesystem</th>
                                <th>Size</th>
                                <th>Used</th>
                                <th>Avail</th>
                                <th>Use%</th>
                                <th>Mounted</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>/dev/mapper/VolGroup-lv_root</td>
                                <td>8.3G</td>
                                <td>3.9G</td>
                                <td>4.1G</td>
                                <td>
                                    <div class="">
                                        <div class="progress progress_sm" style="width: 76%;">
                                            <div class="progress-bar bg-green" role="progressbar"
                                                 data-transitiongoal="80"></div>
                                        </div>
                                    </div>
                                </td>
                                <td>/</td>
                            </tr>
                            <tr>
                                <td>/dev/mapper/VolGroup-lv_root</td>
                                <td>8.3G</td>
                                <td>3.9G</td>
                                <td>4.1G</td>
                                <td>
                                    <div class="">
                                        <div class="progress progress_sm" style="width: 76%;">
                                            <div class="progress-bar bg-green" role="progressbar"
                                                 data-transitiongoal="80"></div>
                                        </div>
                                    </div>
                                </td>
                                <td>/</td>
                            </tr>
                            <tr>
                                <td>/dev/mapper/VolGroup-lv_root</td>
                                <td>8.3G</td>
                                <td>3.9G</td>
                                <td>4.1G</td>
                                <td>
                                    <div class="">
                                        <div class="progress progress_sm" style="width: 76%;">
                                            <div class="progress-bar bg-green" role="progressbar"
                                                 data-transitiongoal="80"></div>
                                        </div>
                                    </div>
                                </td>
                                <td>/</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                <!--/Disk Details-->
                <!--Services Status-->
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Services Status <a href="" class="blue">(chkconfig auto start up)</a></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <!--<li class="dropdown">-->
                            <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>-->
                            <!--<ul class="dropdown-menu" role="menu">-->
                            <!--<li><a href="#">Settings 1</a>-->
                            <!--</li>-->
                            <!--<li><a href="#">Settings 2</a>-->
                            <!--</li>-->
                            <!--</ul>-->
                            <!--</li>-->
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="table-responsive x_content">

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Apache Webserver</td>
                                <td><p><i class="fa fa-square orange"></i> IOS </p></td>
                                <td>
                                    <button type="button" class="btn btn-success">
                                        <a href="#/play"><i
                                                    class="fa fa-play"></i>
                                        </a> Play
                                    </button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-danger">
                                        <a href="#/stop"><i
                                                    class="fa fa-stop"></i>
                                        </a> Stop
                                    </button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-warning">
                                        <a href="#/refresh"><i
                                                    class="fa fa-refresh"></i>
                                        </a> Restart
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td>FTP Server</td>
                                <td><p><i class="fa fa-square green"></i> httpd (pid 1604) is running... </p>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-success">
                                        <a href="#/play"><i
                                                    class="fa fa-play"></i>
                                        </a> Play
                                    </button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-danger">
                                        <a href="#/stop"><i
                                                    class="fa fa-stop"></i>
                                        </a> Stop
                                    </button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-warning">
                                        <a href="#/refresh"><i
                                                    class="fa fa-refresh"></i>
                                        </a> Restart
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td>MySQL Database Server</td>
                                <td><p><i class="fa fa-square red"></i> clamd is stopped </p></td>
                                <td>
                                    <button type="button" class="btn btn-success">
                                        <a href="#/play"><i
                                                    class="fa fa-play"></i>
                                        </a> Play
                                    </button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-danger">
                                        <a href="#/stop"><i
                                                    class="fa fa-stop"></i>
                                        </a> Stop
                                    </button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-warning">
                                        <a href="#/refresh"><i
                                                    class="fa fa-refresh"></i>
                                        </a> Restart
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Mail Server Services</th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Apache Webserver</td>
                                <td><p><i class="fa fa-square orange"></i> IOS </p></td>
                                <td>
                                    <button type="button" class="btn btn-success">
                                        <a href="#/play"><i
                                                    class="fa fa-play"></i>
                                        </a> Play
                                    </button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-danger">
                                        <a href="#/stop"><i
                                                    class="fa fa-stop"></i>
                                        </a> Stop
                                    </button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-warning">
                                        <a href="#/refresh"><i
                                                    class="fa fa-refresh"></i>
                                        </a> Restart
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td>FTP Server</td>
                                <td><p><i class="fa fa-square green"></i> httpd (pid 1604) is running... </p>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-success">
                                        <a href="#/play"><i
                                                    class="fa fa-play"></i>
                                        </a> Play
                                    </button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-danger">
                                        <a href="#/stop"><i
                                                    class="fa fa-stop"></i>
                                        </a> Stop
                                    </button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-warning">
                                        <a href="#/refresh"><i
                                                    class="fa fa-refresh"></i>
                                        </a> Restart
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td>MySQL Database Server</td>
                                <td><p><i class="fa fa-square red"></i> clamd is stopped </p></td>
                                <td>
                                    <button type="button" class="btn btn-success">
                                        <a href="#/play"><i
                                                    class="fa fa-play"></i>
                                        </a> Play
                                    </button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-danger">
                                        <a href="#/stop"><i
                                                    class="fa fa-stop"></i>
                                        </a> Stop
                                    </button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-warning">
                                        <a href="#/refresh"><i
                                                    class="fa fa-refresh"></i>
                                        </a> Restart
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                <!--/Services Status-->
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
                <!--Firewall Backups Support-->
                <div class="x_panel" style="text-align: center;">
                    <div class="col-xs-6">
                        <a class="btn btn-app">
                            <span class="badge bg-red">OFF</span>
                            <i class="fa fa-shield"></i> Firewall
                        </a>
                    </div>
                    <div class="col-xs-6">
                        <a class="btn btn-app">
                            <span class="badge bg-green">ON</span>
                            <i class="fa fa-database"></i> Backups
                        </a>
                    </div>
                    <div class="col-xs-12">
                        <a class="btn btn-app">
                            <span class="badge bg-green">ON</span>
                            <i class="fa fa-support"></i> Support
                        </a>
                    </div>
                </div>
                <!--/Firewall Backups Support-->
                <!--System Stats-->
                <div class="x_panel tile">
                    <div class="x_title">
                        <h2>System Stats</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div>
                            <p>Memory RAM (with Cache): <strong class="orange">0.36GB / 1GB ( 36%) </strong><a href=""
                                                                                                               class="blue">[DC]</a>
                            </p>
                            <div class="">
                                <div class="progress progress_sm">
                                    <div class="progress-bar bg-green" role="progressbar"
                                         data-transitiongoal="80"></div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <p>Memory RAM (NO Cache): <strong class="orange">0.24GB / 1GB ( 24%) </strong></p>
                            <div class="">
                                <div class="progress progress_sm">
                                    <div class="progress-bar bg-green" role="progressbar"
                                         data-transitiongoal="60"></div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <p>Number of processes: <strong class="orange">109 </strong></p>
                        </div>
                        <div>
                            <p>Postfix Mail Queue: <strong class="orange">0 </strong><a href=""
                                                                                        class="blue">[Manage]</a></p>
                        </div>
                    </div>
                </div>
                <!--/System Stats-->
                <!--Application Version-->
                <div class="x_panel tile">
                    <div class="x_title">
                        <h2>Application Version</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div>
                            <p>Apache version: <strong class="orange">Apache/2.2.27 </strong></p>
                        </div>
                        <div>
                            <p>PHP version: <strong class="orange">5.4.45 </strong><a href="" class="blue">[PHP
                                    Switcher]</a></p>
                        </div>
                        <div>
                            <p>MySQL version: <strong class="orange">10.1.21-MariaDB </strong></p>
                        </div>
                        <div>
                            <p>FTP version: <strong class="orange">1.0.36 </strong></p>
                        </div>
                    </div>
                </div>
                <!--/Application Version-->
                <!--System Info-->
                <div class="x_panel tile">
                    <div class="x_title">
                        <h2>System Info</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div>
                            <p>CPU Model: <strong class="orange">Intel(R) Core(TM) i7-3632QM CPU @ 2.20GHz </strong></p>
                        </div>
                        <div>
                            <p>CPU Details: <strong class="orange">1 Core (2201 MHz) </strong></p>
                        </div>
                        <div>
                            <p>Distro Name: <strong class="orange">CentOS release 6.8 (Final) </strong></p>
                        </div>
                        <div>
                            <p>Kernel Version: <strong class="orange">2.6.32-642.3.1.el6.x86_64 </strong></p>
                        </div>
                        <div>
                            <p>Platform: <strong class="orange">x86_64 [vmware] </strong></p>
                        </div>
                        <div>
                            <p>Uptime: <strong class="orange">11:21, 1 user </strong></p>
                        </div>
                        <div>
                            <p>Server Time: <strong class="orange">Mon Feb 20 20:42:21 PKT 2017 </strong><a href=""
                                                                                                            class="blue">[Edit]</a>
                            </p>
                        </div>
                    </div>
                </div>
                <!--/System Info-->
                <!--CWP Info-->
                <div class="x_panel tile">
                    <div class="x_title">
                        <h2>CWP Info</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div>
                            <p><strong class="dark">NS1: </strong>ns1.centos-webpanel.com | <strong
                                        class="dark">IP: </strong>127.0.0.1 <a href="" class="blue">[Change]</a></p>
                        </div>
                        <div>
                            <p><strong class="dark">NS2: </strong>ns2.centos-webpanel.com | <strong
                                        class="dark">IP: </strong>127.0.0.1 <a href="" class="blue">[Change]</a></p>
                        </div>
                        <div>
                            <p><strong class="dark">Server IP: </strong>192.168.80.128 | 192.168.80.128</p>
                        </div>
                        <div>
                            <p><strong class="dark">Shared IP: </strong>122.129.73.245 <a href=""
                                                                                          class="blue">[Change]</a></p>
                        </div>
                        <div>
                            <p><strong class="dark">Hostname: </strong>localhost.localdomain <a href="" class="blue">[Change]</a>
                            </p>
                        </div>
                        <div>
                            <p><strong class="dark">Your IP: </strong>192.168.80.1</p>
                        </div>
                        <div>
                            <p><strong class="dark">CWP version: </strong>0.9.8.153 <a href="" class="blue">[Get
                                    CWPpro]</a></p>
                        </div>
                    </div>
                </div>
                <!--/CWP Info-->
            </div>
        </div>


        <div class="clearfix"></div>

    </div>
</div>

@include('templates.footer')