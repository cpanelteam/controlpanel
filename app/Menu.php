<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Menu extends Model
{
    public static function getMenuByUserType($type_id){
        if($type_id == 1){
            $result = DB::table('menu')
                ->get();
            return $result;
        }else{
            $result = DB::table('user_menu')
                ->select('menu.*')
                ->rightJoin('menu', 'user_menu.menu_id', '=', 'menu.menu_id')
                ->where('user_type_id', $type_id)
                ->get();
            return $result;
        }
    }

    public static function deleteMenuByUserType($type_id){
        DB::table('user_menu')
            ->where('user_type_id', $type_id)
            ->delete();
    }

    public static function saveMenuByUserType($type_id, $menu_id){
        DB::table('user_menu')->insert(
            ['menu_id' => $menu_id, 'user_type_id' => $type_id]
        );
    }

    public static function getTransferableMenu(){
        $result = DB::table('menu')
            ->where('is_transferable', 1)
            ->get();
        return $result;
    }
}
