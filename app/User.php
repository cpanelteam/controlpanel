<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function getUserById($user_id){
        $result = DB::table('users')
            ->where('id', $user_id)
            ->first();
        return $result;
    }

    public static function saveNewUser($userObject){
        DB::table('users')->insert([
            'name' => $userObject->name,
            'username' => $userObject->username,
            'password' => $userObject->password,
            'email' => $userObject->email,
            'plain_password' => $userObject->plain_password,
            'server_ip' => $userObject->server_ips,
            'type_id' => $userObject->user_type,
            'package_id' => $userObject->package,
            'inode' => $userObject->inode,
            'has_shellaccess' => $userObject->shell_access,
            'has_backup' => $userObject->backup,
            'no_of_processes' => $userObject->processes,
            'no_of_openfiles' => $userObject->open_files
        ]);
    }
}
