<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserTypeDashboard extends Model
{
    public static function getGraphByUserType($user_type_id){
        $result = DB::table('user_type_dashboard')
            ->where('user_type_id', $user_type_id)
            ->get();
        return $result;
    }

    public static function deleteGraphByUserType($type_id){
        DB::table('user_type_dashboard')
            ->where('user_type_id', $type_id)
            ->delete();
    }

    public static function saveGraphByUserType($type_id, $graph_url){
        DB::table('user_type_dashboard')->insert(
            ['user_type_dashboard_value' => $graph_url, 'user_type_id' => $type_id]
        );
    }
}
