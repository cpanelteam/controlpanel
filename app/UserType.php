<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserType extends Model
{
    public static function getAllUserTypes()
    {
        $result = DB::table('user_types')
            ->get();
        return $result;

    }
    public static function addNewUserType($type_name)
    {
        DB::table('user_types')->insert(
            ['user_type_name' => $type_name]
        );
    }
}
