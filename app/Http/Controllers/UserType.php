<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Validator;
use Illuminate\Support\Facades\Input;
use Hash;
use Auth;

class UserType extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($errors = null, $userObject = null)
    {
        $user = Auth::user();
        $menu = App\Menu::getMenuByUserType($user->type_id);
        $roles = App\UserType::getAllUserTypes();
        return view('useraccounts/listroles')
            ->with('menu', $menu)
            ->with('roles', $roles);
    }

    public function getDashboardGraph(){
        $user = Auth::user();
        $graphs = App\UserTypeDashboard::getGraphByUserType(Input::get('user_type_id'));
        return json_encode($graphs);
    }

    public function getRoleMenu(){
        $menu = App\Menu::getMenuByUserType(Input::get('user_type_id'));
        return json_encode($menu);
    }

    public function getMenuList(){
        $menu = App\Menu::getTransferableMenu();
        return json_encode($menu);
    }

    public function saveRoleMenu(){
        if(Input::get('user_type_id') > 1) {
            App\Menu::deleteMenuByUserType(Input::get('user_type_id'));
            if(!empty(Input::get('menu_id'))) {
                foreach (Input::get('menu_id') as $menu_id) {
                    App\Menu::saveMenuByUserType(Input::get('user_type_id'), $menu_id);
                }
            }
        }
        return json_encode(Input::get('user_type_id'));
    }

    public function saveDashboardGraph(){
        App\UserTypeDashboard::deleteGraphByUserType(Input::get('user_type_id'));
        if(!empty(Input::get('graph_urls'))) {
            foreach (Input::get('graph_urls') as $graph_url) {
                App\UserTypeDashboard::saveGraphByUserType(Input::get('user_type_id'), $graph_url);
            }
        }
        return json_encode(Input::get('user_type_id'));
    }

    public function newRoleForm(){
        $user = Auth::user();
        $menu = App\Menu::getMenuByUserType($user->type_id);
        return view('useraccounts/newroles')
            ->with('menu', $menu);
    }

    public function addNewRole(){
        App\UserType::addNewUserType(Input::get('user_type_name'));
        return redirect('useraccounts/listroles');
    }

    public function printResult($str){
        echo "<pre>";
        print_r($str);
        echo "</pre>";
        die();
    }
}
