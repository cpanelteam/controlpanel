<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Auth;

class PackageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $menu = App\Menu::getMenuByUserType($user->type_id);
        $graphs = App\UserTypeDashboard::getGraphByUserType($user->type_id);

        return view('dashboard')
            ->with('menu', $menu)
            ->with('customize', false)
            ->with('graphs', $graphs);
    }

    public function newPackageForm(){
        $user = Auth::user();
        $menu = App\Menu::getMenuByUserType($user->type_id);
        return view('packages/newpackage')
            ->with('menu', $menu);
    }

    public function listPackages(){
        $user = Auth::user();
        $menu = App\Menu::getMenuByUserType($user->type_id);
        return view('packages/listpackages')
            ->with('menu', $menu);
    }

    public function printResult($str){
        echo "<pre>";
        print_r($str);
        echo "</pre>";
        die();
    }
}
