<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Validator;
use Illuminate\Support\Facades\Input;
use Hash;
use Auth;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($errors = null, $userObject = null)
    {
        $user = Auth::user();
        $menu = App\Menu::getMenuByUserType($user->type_id);
        return view('useraccounts/listaccounts')
            ->with('menu', $menu);
    }

    public function newAccountForm($errors = null, $userObject = null)
    {
        $user = Auth::user();
        $menu = App\Menu::getMenuByUserType($user->type_id);
        $user_types = App\UserType::getAllUserTypes();
        return view('useraccounts/newaccount')
            ->with('menu', $menu)
            ->with('user_types', $user_types)
            ->with('errors', $errors)
            ->with('userObject', $userObject);
    }
    public function addNewAccount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'email|required|unique:users',
            'password' => 'required|min:8',
            'username' => 'required|unique:users|min:5',
            'fullname' => 'required|min:5',
            'user_type' => 'required',
            'server_ips' => 'required',
            'package' => 'required',
            'inode' => 'required',
            //'backup' => 'required',
            'processes' => 'required',
            'open_files' => 'required',
        ]);

        $userObject = new \stdClass();

        $userObject->name = Input::get('fullname');
        $userObject->username = trim(Input::get('username'));
        $userObject->password = Hash::make(Input::get('password'));
        $userObject->email = trim(Input::get('email'));
        $userObject->plain_password = Input::get('password');
        $userObject->user_type = Input::get('user_type');
        $userObject->server_ips = Input::get('server_ips');
        $userObject->package = Input::get('package');
        $userObject->inode = Input::get('inode');
        $userObject->shell_access = Input::get('shell_access') ? Input::get('shell_access') : 0;
        $userObject->backup = Input::get('backup') ? Input::get('backup') : 0;
        $userObject->processes = Input::get('processes');
        $userObject->open_files = Input::get('open_files');

        if ($validator->fails()) {
            return $this->index($validator->errors(), $userObject);
        }
        App\User::saveNewUser($userObject);
        return redirect('dashboard');
    }

    public function printResult($str){
        echo "<pre>";
        print_r($str);
        echo "</pre>";
        die();
    }
}
